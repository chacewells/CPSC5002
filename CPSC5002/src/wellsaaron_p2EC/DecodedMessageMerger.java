/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_p2EC;

import java.io.File;
import java.util.Iterator;

/**
 * Short description of the class.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class DecodedMessageMerger {
	private MessageDecoder firstDecoder, secondDecoder;
	private EncodedCharNode head, tail;
	
	private DecodedMessageMerger(
			MessageDecoder firstDecoder,
			MessageDecoder secondDecoder) {
		this.firstDecoder = firstDecoder;
		this.secondDecoder = secondDecoder;
	}
	
	public static void main(String[] args) {
		MessageDecoder d1 = new MessageDecoder();
		d1.populateWithFileContents(new File("msg1.dat"));
		MessageDecoder d2 = new MessageDecoder();
		d2.populateWithFileContents(new File("msg2.dat"));
		
		DecodedMessageMerger merger = new DecodedMessageMerger(d1, d2);
		merger.mergeMessages();
		
		System.out.println(merger.getPlaintextMessage());
	}
	
	enum WhichItr { FIRST, SECOND }
	void mergeMessages() {
		Iterator<EncodedChar> firstItr = firstDecoder.iterator();
		Iterator<EncodedChar> secondItr = secondDecoder.iterator();
		WhichItr which = WhichItr.FIRST;
		
		while (firstItr.hasNext() || secondItr.hasNext()) {
			switch (which) {
			case FIRST:
				which = handleNext(firstItr, which);
				break;
			case SECOND:
				which = handleNext(secondItr, which);
				break;
			}
		}
	}
	
	WhichItr handleNext(Iterator<EncodedChar> itr, WhichItr currentWhich) {
		if (itr.hasNext()) {
			EncodedChar next = itr.next();
			add(next);
			if (next.character == ' ') {
				return switchWhich(currentWhich);
			} else if (!itr.hasNext()) {
				add(EncodedChar.parseLine("  1"));
				return switchWhich(currentWhich);
			} else {
				return currentWhich;
			}
		} else {
			return switchWhich(currentWhich);
		}
	}
	
	static WhichItr switchWhich(WhichItr which) {
		return which == WhichItr.FIRST 
				? WhichItr.SECOND
				: WhichItr.FIRST;
	}
	
	String getPlaintextMessage() {
		StringBuilder builder = new StringBuilder();
		EncodedCharNode current = head;
		while (current != null) {
			builder.append(current.data.character);
			current = current.next;
		}
		
		return builder.toString();
	}
	
	void add(EncodedChar encodedChar) {
		if (empty()) {
			head = new EncodedCharNode(new EncodedChar(encodedChar));
			tail = head;
		} else {
			tail.next = new EncodedCharNode(new EncodedChar(encodedChar));
			tail = tail.next;
		}
	}
	
	boolean empty() {
		return head == null;
	}

}
