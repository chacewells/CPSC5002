/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_p2EC;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Decodes a scrambled message consisting of lines with format "c i" where
 * 'c' is a single ASCII character and 'i' is an index representing the proper
 * order of the character in the decoded message.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class MessageDecoder {
	// The message, parsed into list nodes of EncodedChar. May or may not be
	// properly ordered.
	private EncodedCharNode messageList;
	
	/**
	 * Returns the decoded message.
	 * 
	 * @return The decoded message.
	 */
	public String getPlainTextMessage() {
		// Holds the contents of the message.
		StringBuilder builder = new StringBuilder();
		
		EncodedCharNode current = messageList;
		while (current != null) {
			builder.append(current.data.character);
			current = current.next;
		}
		
		return builder.toString();
	}
	
	/**
	 * Populates this decoder with the file contents in their decoded order.
	 * Precondition: The file exists and is a regular file.
	 * 
	 * @param file The file from which to read encoded contents.
	 */
	public void populateWithFileContents(File file) {
		// Holds the file contents as each line is read
		String line;
		
		// clear the current list
		clear();
		
		try (
			// closes implicitly; see https://tinyurl.com/nggbv2b
			BufferedReader reader = new BufferedReader(
					new FileReader(file));
				) {
			for (line = reader.readLine();
					line != null;
					line = reader.readLine()) {
				addOrdered(line);
			}
		} catch (IOException e) {
			// IO is properly handled above, and file is expected to exist,
			// so this would be unexpected; rethrowing as unchecked.
			throw new RuntimeException(e);
		}
		
	 /*
	  *  Ensure there are no repeated lines.
	  */
		removeDuplicates();
	}
	
	// empties the list backing this decoder
	private void clear() {
		messageList = null;
	}
	
	// Inserts a value in ascending order.
	private void addOrdered(String line) {
		// The line, parsed
		EncodedChar encoded;
		// The node to add
		EncodedCharNode newNode = null;
		// Alias for iterating over messageList
		EncodedCharNode current;
		
		if (!EncodedChar.isLineValid(line)) {
			System.err.println("The line is not valid: " + line);
			System.err.println("Omitting it from the message.");
			return;
		}
		encoded = EncodedChar.parseLine(line);
	
		if (isEmpty()) {
			messageList = new EncodedCharNode(encoded);
		} else if (encoded.compareTo(messageList.data) <= 0){
			// newNode links to first
			newNode = new EncodedCharNode(
					encoded, 
					messageList);
			// newNode is now the first element
			messageList = newNode;
		} else {
			// alias of first for iteration
			current = messageList;
			while (current.next != null 
					&& encoded.compareTo(current.next.data) > 0) {
				current = current.next;
			}
			
			// place newNode between current and current.next
			newNode = new EncodedCharNode(
					encoded, 
					current.next);
			current.next = newNode;
		}
	}
	
	/*
	 * Removes duplicate elements from a singly linked list.
	 * NOTE: Exact duplicate lines are considered OKAY (e.g. two lines that
	 * both read "* 19"); one will simply be removed.
	 * However, lines containing the same line index, but
	 * different characters is not OKAY, since that indicates an ambiguity
	 * in which character is supposed to be placed in that position.
	 */ 
	private void removeDuplicates() {
		// alias node for iteration
		EncodedCharNode current = messageList;
		
		if (isEmpty()) {
			return; // nothing to remove; also acts as a null guard
		}
		
		while (current.next != null) {
			if (current.data.equals(current.next.data)) {
				current.next = current.next.next;
				continue;
			}
			
			current = current.next;
		}
	}
	
	/**
	 * Validates that any duplicate character indexes have matching characters.
	 * <p>
	 * NOTE: Exact duplicate lines are considered OKAY (e.g. two lines that
	 * both read "* 19"); one will simply be removed.
	 * However, lines containing the same line index, but
	 * different characters is not OKAY, since that indicates an ambiguity
	 * in which character is supposed to be placed in that position.
	 * 
	 * @return True if all duplicate indexes correspond to duplicate characters;
	 * false otherwise.
	 */ 
	public boolean isMessageValid() {
		// alias node for iteration
		EncodedCharNode current = messageList;
		
		if (isEmpty()) {
			return true; // nothing to remove; also acts as a null guard
		}
		
		while (current.next != null) {
			if (current.data.index == current.next.data.index
					&& current.data.character != current.next.data.character) {
				return false;
			}
			
			current = current.next;
		}
		
		return true;
	}
	
	// indicates whether the list backing this decoder is empty
	// no need for public; this is only used internally
	private boolean isEmpty() {
		return messageList == null;
	}
	
	public Iterator<EncodedChar> iterator() {
		return new EncodedCharIterator();
	}
	
	private class EncodedCharIterator implements Iterator<EncodedChar> {
		private EncodedCharNode current = messageList;

		@Override
		public boolean hasNext() {
			return !empty();
		}

		@Override
		public EncodedChar next() {
			EncodedChar next;
			
			if (empty()) {
				throw new NoSuchElementException("Links exhausted");
			}
			
			next = new EncodedChar(current.data);
			current = current.next;
			
			return next;
		}
		
		private boolean empty() {
			return current == null;
		}
		
	}
	
}
