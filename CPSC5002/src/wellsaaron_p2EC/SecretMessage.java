/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_p2EC;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Program to decode a secret message. Prompts the user for a file name or path,
 * reads the encoded file, and displays the decoded message to the user in
 * plaintext. For details on the line format, 
 * see {@link EncodedChar#parseLine(String)}.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class SecretMessage {
	
	/**
	 * See class header.
	 * 
	 * @param args No program arguments are required.
	 */
	public static void main(String[] args) throws IOException {
		// The message decoder.
		MessageDecoder decoder = new MessageDecoder();
		// Indicates whether the user is done decoding message files.
		boolean done = false;
		// The path of the file to decode.
		String filename = null;
		// The file to decode.
		File file = null;
		
		try (
			/*
			 * Reader for user input.
			 * Closed implicitly; see https://tinyurl.com/nggbv2b
			 */
			BufferedReader console = getConsole();
				) {
			while (!done) {
				filename = getFilenameFromUser(console);
				
				file = new File(filename);
				if (!file.isFile()) {
					displayInvalidFileMessage(filename);
					continue;
				}
				
				displayDecodedMessage(decoder, file);
				done = promptUserForDone(console);

				// Line separator for next round
				System.out.println();
			}
			
			System.out.println("Goodbye!");
		}
	}
	
	// Displays the decoded text message.
	private static void displayDecodedMessage(
			MessageDecoder decoder, 
			File file) {
		decoder.populateWithFileContents(file);
		
		if (!decoder.isMessageValid()) {
			System.out.println("The message was not valid. "
					+ "Duplicate indexes must have the same character.\n");
			return;
		}
		
		System.out.println("Here is the message:");
		System.out.println(decoder.getPlainTextMessage() + "\n");
	}
	
	// Prompts the user for the path of a file to decode
	private static String getFilenameFromUser(BufferedReader console) 
			throws IOException {
		System.out.print("Please enter the filename or path of the file to "
				+ "decode: ");
		return console.readLine();
	}
	
	// Informs the user that the file is not valid.
	private static void displayInvalidFileMessage(String filename) {
		System.out.println("Sorry, '" + filename + "' either doesn't exist or "
				+ "is not a regular file.");
		System.out.println("Please enter a valid file path.\n");
	}
	
	// Asks the user whether to decode another file.
	// If user responds "y", returns false for done, otherwise true.
	private static boolean promptUserForDone(BufferedReader console)
			throws IOException {
		System.out.print("Would you like to decode another file (y/n)? ");
		
		// The user input
		String userInput = console.readLine();
		
		// Whether the user wants to decode another file
		boolean decodeAgain = userInput.toLowerCase().contains("y");
		
		return !decodeAgain;
	}
	
	// Wrap standard input in a BufferedReader
	private static BufferedReader getConsole() throws IOException {
		return new BufferedReader(new InputStreamReader(System.in));
	}

}
