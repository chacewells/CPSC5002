/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_p2EC;

/**
 * Represents an element in a single-linked data structure. Contains a data 
 * element and a reference to the next node.
 * <p>
 * Note: This class and its API are package private as it will only be used
 * by MessageDecoder in this package. The instance fields are also
 * left package-private for convenience and in keeping with the pattern for
 * list nodes that has been established in classroom instruction.
 * 
 * @author Aaron Wells
 *
 */
class EncodedCharNode {
	// This node's data
	EncodedChar data;
	// A reference to the next node
	EncodedCharNode next;
	
	/**
	 * Creates a new linked node populated with a value and a reference to the
	 * next node in the data structure.
	 * 
	 * @param data The value.
	 * @param next A reference to the next node.
	 */
	EncodedCharNode(EncodedChar data, EncodedCharNode next) {
		this.data = new EncodedChar(data);
		this.next = next;
	}
	
	/**
	 * Convenience constructor for node with no successor.
	 * @param data The value.
	 */
	EncodedCharNode(EncodedChar data) {
		this(data, null);
	}
}