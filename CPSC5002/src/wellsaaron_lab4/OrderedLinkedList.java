/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_lab4;

/**
 * A linked list implementation that stores its contents in ascending order.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class OrderedLinkedList {
	private ListNode first; // The first node in this list.
	
	/**
	 * Initializes an empty linked list.
	 */
	public OrderedLinkedList() {
		first = null; // initializes empty
	}
	
	/**
	 * Inserts a value in ascending order.
	 * @param data The number to insert.
	 */
	public void addOrdered(int data) {
		if (first == null) {
			first = new ListNode(data);
		} else if (data <= first.data){
			// newNode links to first
			ListNode newNode = new ListNode(data, first);
			// newNode is now the first element
			first = newNode;
		} else {
			// alias of first for iteration
			ListNode current = first;
			while (current.next != null && data > current.next.data) {
				current = current.next;
			}
			
			// place newNode between current and current.next
			ListNode newNode = new ListNode(data, current.next);
			current.next = newNode;
		}
	}
	
	/**
	 * Creates a new iterator on this list.
	 * @return The iterator.
	 */
	public OrderedLinkedListIterator iterator() {
		return new OrderedLinkedListIterator(first);
	}
}
