/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_lab4;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Reads a file of integers into a linked list in ascending order, then
 * presents the result to standard output.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class Lab4 {

	/**
	 * See class JavaDoc.
	 * 
	 * @param args Command-line arguments.
	 * @throws IOException If an error occurs opening the file or reading input.
	 */
	public static void main(String[] args) 
			throws IOException {
		// The file to read
		final String fileName = "lab4.dat";
		// The list to build and display
		OrderedLinkedList list;
		
		try (
			// Reads file contents
			BufferedReader reader = getBufferedReader(fileName);
		) {
			list = buildOrderedListFromReader(reader);
			displayList(list);
		}
	}
	
	// Prints the contents of a linked list to standard output with each data
	// point on a separate line.
	private static void displayList(OrderedLinkedList list) {
		int valueToPrint; // The number to display
		OrderedLinkedListIterator itr = list.iterator();
		
		while (itr.hasNext()) {
			valueToPrint = itr.next();
			System.out.println(valueToPrint);
		}
	}
	
	// Returns the contents of the input in a linked list of integers in 
	// ascending order.
	private static OrderedLinkedList buildOrderedListFromReader(
			BufferedReader reader) throws IOException {
		String line; // Stores each line of input
		int lineInteger; // The line data as integer
		OrderedLinkedList list = new OrderedLinkedList(); // The list to build
		
		line = reader.readLine();
		while (line != null) {
			lineInteger = Integer.parseInt(line);
			list.addOrdered(lineInteger);
			
			line = reader.readLine();
		}
		
		return list;
	}
	
	// Loads the so-named file into a BufferedReader.
	private static BufferedReader getBufferedReader(String filePath) 
			throws IOException {
		return new BufferedReader(new FileReader(filePath));
	}
	
}

