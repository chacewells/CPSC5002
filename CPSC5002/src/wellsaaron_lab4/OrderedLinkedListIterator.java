/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_lab4;

/**
 * Iterates over the elements of an {@link OrderedLinkedList}.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class OrderedLinkedListIterator {
	private ListNode next; // Reference to iterate over list nodes
	
	/**
	 * Constructs a new iterator initialized with the first element in the list.
	 * @param head The first element to be iterated on.
	 */
	OrderedLinkedListIterator(ListNode head) {
		next = head;
	}
	
	/**
	 * Whether this iterator contains any more elements.
	 * @return True if next() will yield another integer; false otherwise.
	 */
	public boolean hasNext() {
		return next != null;
	}
	
	/**
	 * Returns the next element and advances the iterator.
	 * @return The next element.
	 * @throws IllegalStateException if there are no more elements.
	 */
	public int next() {
		validateCanIterate();
		int value = next.data; // the next value
		next = next.next; // advance the iterator
		
		return value;
	}
	
	// throws an exception if there are no more elements
	private void validateCanIterate() {
		if (!hasNext()) {
			throw new IllegalStateException("This iterator has been exhausted");
		}
	}
}
