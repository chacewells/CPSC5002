/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_lab5;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Reads a file of integers into a linked list in ascending order, 
 * prints the result to standard output. It then removes duplicates from the
 * list and prints the list of unique values to standard output. 
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class Lab5 {

	/**
	 * See class heading.
	 * 
	 * @param args Command-line arguments.
	 * @throws IOException If an error occurs opening the file or reading input.
	 * @throws URISyntaxException If the resource file cannot be found.
	 */
	public static void main(String[] args) 
			throws IOException {
		// The file to read
		String fileName = "lab5.dat";
		// The list to build and display
		ListNode list;
		
		try (
/* Reads file contents
 * Implicitly closed.
 * See https://tinyurl.com/nggbv2b
 */
			BufferedReader reader = getBufferedReader(fileName);
				) {
			list = buildOrderedListFromReader(reader);
			System.out.println("LIST BEFORE DUPLICATE REMOVAL");
			displayList(list);
			
			removeDuplicates(list);
			System.out.println("LIST AFTER DUPLICATE REMOVAL");
			displayList(list);
		}
	}
	
	// Prints the contents of a linked list to standard output with each data
	// point on a separate line.
	private static void displayList(ListNode list) {
		// alias of list for iteration
		ListNode current = list;
		
		while (current != null) {
			System.out.println(current.data);
			current = current.next;
		}
	}
	
	// Returns the contents of the input in a linked list of integers in 
	// ascending order.
	private static ListNode buildOrderedListFromReader(BufferedReader reader)
			throws IOException {
		// Contents of each line as iterated over by reader
		String line;
		// The line data as integer
		int lineInteger;
		// The list to build
		ListNode list;
		
		line = reader.readLine();
		list = new ListNode(Integer.parseInt(line));
		
		while (line != null) {
			lineInteger = Integer.parseInt(line);
			insertOrdered(list, lineInteger);
			
			line = reader.readLine();
		}
		
		return list;
	}
	
	// Inserts an integer into a linked list before the next largest element.
	private static void insertOrdered(ListNode node, int newData) {
		// alias for node; to iterate over node's contents
		ListNode ref;
		
		ref = node;
		
		// validate node is not a null reference
		if (ref == null) {
			System.out.println("insertOrdered(): Cannot insert into a null "
					+ "reference.");
			return;
		}
		
		// newData is less than the current node's
		if (newData < ref.data) {
			insertBefore(ref, newData);
			return;
		}
		
		while (ref.next != null && newData > ref.next.data) {
			ref = ref.next;
		}
		
		// newData is greater than node.next.data
		// link a new node to node.next
		insertAfter(ref, newData);
	}
	
	// Inserts an integer before the node.
	private static void insertBefore(ListNode node, int newData) {
		// contains ListNode for newData
		ListNode newNode;
		
		// newNode gets node's data
		// newNode and node point to  second node
		newNode = new ListNode(node.data, node.next);
		
		// replace node's data with newData
		node.data = newData;
		
		// now point list to newNode
		node.next = newNode;
	}
	
	// Inserts an integer after the node.
	private static void insertAfter(ListNode node, int newData) {
		// newNode linked to node.next
		ListNode newNode;
		
		newNode = new ListNode(newData, node.next);
		
		// relink node.next to newNode
		node.next = newNode;
	}
	
	// Removes duplicate elements from a singly linked list.
	private static void removeDuplicates(ListNode list) {
		// alias node for iteration
		ListNode current = list;
		while (current.next != null) {
			if (current.data == current.next.data) {
				current.next = current.next.next;
				continue;
			}
			
			current = current.next;
		}
	}
	
// Loads the resource from the classpath into a BufferedReader.
	private static BufferedReader getBufferedReader(String filePath) 
			throws IOException {
		return new BufferedReader(new FileReader(filePath));
	}
	
}

