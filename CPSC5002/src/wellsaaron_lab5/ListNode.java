/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_lab5;

/**
 * Represents an element in a single-linked data structure. Contains a data 
 * element and a reference to the next node.
 * 
 * @author Aaron Wells
 *
 */
class ListNode {
	// This node's data
	int data;
	// A reference to the next node
	ListNode next;
	
	/**
	 * Creates a new linked node populated with a value and a reference to the
	 * next node in the data structure.
	 * 
	 * @param data The value.
	 * @param next A reference to the next node.
	 */
	public ListNode(int data, ListNode next) {
		this.data = data;
		this.next = next;
	}
	
	/**
	 * Creates a new linked node populated with a value and no reference to 
	 * another node.
	 * 
	 * @param data The value.
	 */
	public ListNode(int data) {
		this(data, null);
	}
}