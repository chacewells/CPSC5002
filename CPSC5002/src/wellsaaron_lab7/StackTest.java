/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_lab7;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * Per instructions during class, JavaDoc comments are not required for these
 * test case methods.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class StackTest {
	// three elements
	private Stack threeElemStack;
	// two elements
	private Stack twoElemStack;
	// one element
	private Stack oneElemStack;
	// empty stack
	private Stack emptyStack;

	// initialize the stack test data
	@Before
	public void setUp() throws Exception {
		emptyStack = new Stack();
		oneElemStack = emptyStack.push(7);
		twoElemStack = oneElemStack.push(4);
		threeElemStack = twoElemStack.push(5);
	}
	
	// test cases
	
	// empty stack
	// - push
	@Test
	public void pushEmptyStack() {
		assertEquals("Push 7 results in [7].",
				oneElemStack, emptyStack.push(7));
	}
	
	// one element
	// - push
	@Test
	public void pushSingleElemStack() {
		assertEquals("Push 4 results in [4<7]",
				twoElemStack, oneElemStack.push(4));
	}
	
	// - pop
	@Test
	public void popSingleElemStack() {
		assertEquals("Pop results in []",
				emptyStack, oneElemStack.pop());
	}
	
	// - top
	@Test
	public void topSingleElemStack() {
		assertEquals("Top should be 7", 7, oneElemStack.top());
	}
	
	// two elements
	// - push
	@Test
	public void pushTwoElemStack() {
		assertEquals("Push results in [5<4<7]",
				threeElemStack,
				twoElemStack.push(5));
	}
	
	// - pop
	@Test
	public void popTwoElemStack() {
		assertEquals("Pop results in [7]", oneElemStack, twoElemStack.pop());
	}
	
	// - top
	@Test
	public void topTwoElemStack() {
		assertEquals("Top results in 4", 4, twoElemStack.pop());
	}

}
