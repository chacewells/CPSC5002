/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_lab7;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * Unit tests for Queue operations.
 * Per instructions during class, JavaDoc comments are not required for these
 * test case methods.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class QueueTest {
	// three elements
	private Queue threeElemQueue;
	// two elements
	private Queue twoElemQueue;
	// one elements
	private Queue oneElemQueue;
	// empty queue
	private Queue emptyQueue;

	// initialize the queues with test data
	@Before
	public void setUp() throws Exception {
		emptyQueue = new Queue();
		oneElemQueue = emptyQueue.enqueue(7);
		twoElemQueue = oneElemQueue.enqueue(4);
		threeElemQueue = twoElemQueue.enqueue(5);
	}

	// test cases
	
	// empty queue
	// - enqueue
	@Test
	public void enqueueEmptyQueue() {
		assertEquals("Enqueue 7 results in [7].",
				oneElemQueue, emptyQueue.enqueue(7));
	}
	
	// one element
	// - enqueue
	@Test
	public void enqueueSingleElemQueue() {
		assertEquals("Enqueue 4 results in [7,4]",
				twoElemQueue, oneElemQueue.enqueue(4));
	}
	
	// - dequeue
	@Test
	public void dequeueSingleElemQueue() {
		assertEquals("Dequeue results in []",
				emptyQueue, oneElemQueue.dequeue());
	}
	
	// - front
	@Test
	public void frontSingleElemQueue() {
		assertEquals("Front should be 7", 7, oneElemQueue.front());
	}
	
	// two elements
	// - enqueue
	@Test
	public void enqueueTwoElemQueue() {
		assertEquals("Enqueue results in [7,4,5]",
				threeElemQueue,
				twoElemQueue.enqueue(5));
	}
	
	// - dequeue
	@Test
	public void dequeueTwoElemQueue() {
		Queue expected = new Queue().enqueue(5);
		assertEquals("Dequeue results in [5]", expected, twoElemQueue.dequeue());
	}
	
	// - front
	@Test
	public void frontTwoElemQueue() {
		assertEquals("Front results in 7", 7, twoElemQueue.front());
	}
}
