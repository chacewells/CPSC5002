/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */

package wellsaaron_lab7;

/**
 * Created by zontakm on 9/12/2017.
 * This is a dummy implementation of Stack ADT
 */
public class Stack {
    public Stack push(int elt) {return this;}
    public Stack pop() {return this;}
    public int top() {return 0;}
    public boolean isEmpty() {return true;}
}
