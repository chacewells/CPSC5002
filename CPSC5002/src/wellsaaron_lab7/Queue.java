/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */

package wellsaaron_lab7;

/**
 * Created by zontakm on 9/12/2017.
 * This is a DUMMY implementation of Queue ADT
 */
public class Queue {
    public Queue enqueue(int elt) {return this;}
    public Queue dequeue() {return this;}
    public int front() {return 0;}
    public boolean isEmpty() {return true;}
}
