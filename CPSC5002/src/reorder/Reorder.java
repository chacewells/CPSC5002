/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package reorder;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/**
 * Short description of the class.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class Reorder {

	public static void main(String[] args) {
		Queue<Integer> q = new LinkedList<>(
				Arrays.asList(1, 2, -2, 4, -5, 8, -8, 12, -15, 23)
			);
		
		reorder(q);
		
		System.out.println(q);
	}
	
	public static void reorder(Queue<Integer> q) {
		Stack<Integer> help = new Stack<>();
		Integer value;
		int qSize, sSize;
		
		qSize = q.size();
		for (int i = 0; i < qSize; ++i) {
			value = q.remove();
			
			if (value < 0)
				help.push(value);
			else
				q.add(value);
		}
		
		sSize = help.size();
		qSize = q.size();
		for (int i = 0; i < sSize; ++i) {
			value = help.pop();
			q.add(value);
		}
		
		for (int i = 0; i < qSize; ++i) {
			value = q.remove();
			q.add(value);
		}
	}

}
