/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_lab8;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Tests generic Stack and Queue implementations to demonstrate usage of these
 * data structures.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class Lab8 {
	// lookup stream by class; here to facilitate generic implementation of
	// test methods
	private static Map<Class<?>, Stream<?>> generators = new HashMap<>();
	static {
		generators.put(Double.class, doubleGen());
		generators.put(String.class, stringGen());
	}
	
	/**
	 * Runs tests against the two data structures, once each with Strings;
	 * once each with Doubles.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		stackOfStringsTest();
		stackOfDoublesTest();
		queueOfStringsTest();
		queueOfDoublesTest();
	}
	
	// Run the stack test with a string sequence
	private static void stackOfStringsTest() {
		stackTest(String.class);
	}
	
	// Run the stack test with a double sequence
	private static void stackOfDoublesTest() {
		stackTest(Double.class);
	}
	
	// The stack test
	private static <T> void stackTest(Class<T> type) {
		// How many items to test the stack with
		final int stackSize = 20;
		// Generates values to fill the stack with
		// technically not typesafe, but guaranteed to be given the
		// initialization of the generators class field above
		@SuppressWarnings("unchecked")
		Stream<T> generator = (Stream<T>) generators.get(type);
		// Iterator from above stream to populate stack
		Iterator<T> itr;
		// Stacks to test
		Stack<T> a, b;
		System.out.println("Stack of " + type.getSimpleName() + "s...");
		
		a = new Stack<>(stackSize);
		itr = generator.iterator();
		for (int i = 0; i < stackSize; ++i) {
			a.push(itr.next());
		}
		
		System.out.println("Stack: " + a);
		System.out.println("Copy stack to another stack..");
		b = a.copy();
		System.out.println("Stack: " + b);
		System.out.println("Equal? " + (a == b));
		System.out.println("Same contents? " + a.equals(b));
		System.out.println("Empty? " + a.empty());
		System.out.println("Peek: " + a.peek());
		System.out.println("Pop: " + a.pop());
		System.out.print("Pop " + (stackSize - 1) + " times: ");
		for (int i = 0; i < stackSize - 1; ++i) {
			System.out.print(a.pop() + " ");
		}
		System.out.println();
		System.out.println("Empty? " + a.empty());
		System.out.println();
	}
	
	// tests queue implementation with strings
	private static void queueOfStringsTest() {
		queueTest(String.class);
	}
	
	// tests queue implementation with doubles
	private static void queueOfDoublesTest() {
		queueTest(Double.class);
	}
	
	private static <T> void queueTest(Class<T> type) {
		// How many items to test the queue with
		final int stackSize = 20;
		// Generates values to fill the queue with
		// technically not typesafe, but guaranteed to be given the
		// initialization of the generators class field above
		@SuppressWarnings("unchecked")
		Stream<T> generator = (Stream<T>) generators.get(type);
		// Iterator from above stream to populate queue
		Iterator<T> itr;
		
		// Queues to test
		Queue<T> a, b;
		System.out.println("Queue of " + type.getSimpleName() + "s...");
		
		a = new Queue<>();
		itr = generator.iterator();
		for (int i = 0; i < stackSize; ++i) {
			a.enqueue(itr.next());
		}
		
		System.out.println("Queue: " + a);
		System.out.println("Copy queue to another queue..");
		b = a.copy();
		System.out.println("Queue: " + b);
		System.out.println("Equal? " + (a == b));
		System.out.println("Same contents? " + a.equals(b));
		System.out.println("Empty? " + a.empty());
		System.out.println("Peek: " + a.peek());
		System.out.println("Dequeue: " + a.dequeue());
		System.out.print("Dequeue " + (stackSize - 1) + " times: ");
		for (int i = 0; i < stackSize - 1; ++i) {
			System.out.print(a.dequeue() + " ");
		}
		System.out.println();
		System.out.println("Empty? " + a.empty());
		System.out.println();
	}
	
	// generates a stream of doubles from 1 to infinity
	private static Stream<Double> doubleGen() {
		return DoubleStream.iterate(1.0, d -> d + 1.0).mapToObj(Double::valueOf);
	}
	
	// generates a stream of Strings based on a single character,
	// beginning with "!", repeating after 127 (the max ascii char value)
	private static Stream<String> stringGen() {
		int asciiMin = 33, asciiMaxExclusive = 127;
		
		return IntStream.iterate(0, i -> (i + 1))
				.map(i -> i + asciiMin)
				.map(i -> i % asciiMaxExclusive)
				.mapToObj(i -> String.valueOf((char)i));
	}

}
