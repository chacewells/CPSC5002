/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_lab8;

import java.util.Arrays;
import java.util.EmptyStackException;

/**
 * Implements a fixed size stack data structure.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class Stack<E> {
	private E[] stack;
	private int top;
	
	/**
	 * Constructs a fixed-size stack of the specified size.
	 * 
	 * @param size The size of the stack. 
	 */
	@SuppressWarnings("unchecked")
	public Stack(int size) {
		stack = (E[]) new Object[size];
	}
	
	/**
	 * Adds an integer to the stack.
	 * 
	 * @param data The integer to add.
	 * @throws NullPointerException When the user attempts to pass null.
	 * @throws IndexOutOfBoundsException When this stack is full and cannot
	 * accept any more elements.
	 */
	public void push(E data) {
		if (data == null) {
			throw new NullPointerException("Data cannot be null");
		}
		if (full()) {
			throw new IndexOutOfBoundsException("The stack is full");
		}
		
		stack[top++] = data;
	}
	
	/**
	 * Removes and returns the top of the stack.
	 * 
	 * @return The value at the top of the stack.
	 */
	public E pop() {
		E data = peek();
		stack[--top] = null;
		
		return data;
	}
	
	/**
	 * Returns the top of the stack without removing it.
	 * 
	 * @return The top of the stack.
	 * @throws EmptyStackException if this stack is empty.
	 */
	public E peek() {
		if (empty()) {
			throw new EmptyStackException();
		}
		
		return stack[top - 1];
	}
	
	/**
	 * Whether this stack is empty.
	 * 
	 * @return True if the stack is empty; false otherwise.
	 */
	public boolean empty() {
		return top == 0;
	}
	
	/**
	 * Whether this stack is full.
	 * 
	 * @return True if the stack is full of elements.
	 */
	public boolean full() {
		return top == stack.length;
	}
	
	/**
	 * Gets the size of the stack.
	 * 
	 * @return The size of the stack.
	 */
	public int size() {
		return top;
	}
	
	@Override
	public String toString() {
		return Arrays.toString(stack);
	}
	
	// leveraging Arrays.hashCode() to generate a hash
	@Override
	public int hashCode() {
		return Arrays.hashCode(stack);
	}
	
	// leveraging Arrrays.equals to compare equality
	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
		return (obj instanceof Stack<?>)
				? Arrays.equals(stack, ((Stack<?>)obj).stack)
				: false;
	}
	
	// helper method for no-arg copy()
	private void copy(Stack<E> other) {
		Stack<E> temp = new Stack<>(stack.length);
		while (!other.empty()) {
			temp.push(other.pop());
		}
		
		while (!temp.empty()) {
			E data = temp.pop();
			push(data);
			other.push(data);
		}
	}
	
	/**
	 * Build a new Stack object with this stack's contents.
	 * 
	 * @return A new stack containing this stack's contents.
	 */
	public Stack<E> copy() {
		Stack<E> newStack = new Stack<>(stack.length);
		newStack.copy(this);
		
		return newStack;
	}
	
}
