/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_lab8;

import java.util.EmptyStackException;

/**
 * Implements a queue backed by a linked list.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class Queue<E> {
	// the front of the queue
	private Node front;
	// the rear of the queue
	private Node rear;
	
	/**
	 * Adds an element to the rear of this queue.
	 * 
	 * @param data The integer to add.
	 */
	public void enqueue(E data) {
		if (empty()) {
			rear = new Node(data);
			front = rear;
		} else {
			rear.next = new Node(data);
			rear = rear.next;
		}
	}
	
	/**
	 * Removes and returns the element at the front of this queue.
	 * 
	 * @return
	 */
	public E dequeue() {
		if (empty()) {
			throw new EmptyStackException();
		}
		
		E value = front.data;
		front = front.next;
		if (front == null) {
			rear = null;
		}
		
		return value;
	}
	
	/**
	 * Indicates whether the list is empty.
	 * 
	 * @return True if the list is empty; false otherwise.
	 */
	public boolean empty() {
		return rear == null;
	}
	
	/**
	 * Returns the value at the front of the queue, without removing it.
	 * 
	 * @return The value at the front of the queue.
	 */
	public E peek() {
		if (empty()) {
			throw new EmptyStackException();
		}

		return front.data;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder("[");
		Node current = front;
		while (current != null) {
			builder
				.append(String.format("%2d", current.data))
				.append(current.next != null ? ", " : "]");
			
			current = current.next;
		}
		
		return builder.toString();
	}
	
	/**
	 * The number of elements in the queue.
	 * 
	 * @return The size of the queue.
	 */
	public int size() {
		int size = 0;
		Node current = front;
		while (current != null) {
			++size;
			
			current = current.next;
		}
		
		return size;
	}
	
	// helper method for no-arg copy()
	private void copy(Queue<E> other) {
		Node current = other.front;
		
		front = rear = null;
		while (current != null) {
			enqueue(current.data);
			current = current.next;
		}
	}
	
	/**
	 * Builds a new Queue object from this queue's contents.
	 * 
	 * @return A new Queue containing a copy of Queue's contents.
	 */
	public Queue<E> copy() {
		Queue<E> newQueue = new Queue<E>();
		newQueue.copy(this);
		
		return newQueue;
	}
	
	// leverage toString() to generate a hash code
	@Override
	public int hashCode() {
		return toString().hashCode();
	}
	
	// leveraging toString() to compare equality
	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
		
		return (obj instanceof Queue)
				? toString().equals(obj.toString())
				: false;
	}
	
	/**
	 * A node representing an element in the queue.
	 *
	 * @author  Aaron Wells
	 * @version 1.0
	 */
	private class Node {
		// The data to hold
		E data;
		// Link to the next element in the chain
		Node next;
		
		// constructs a Node holding data d and a reference to link n
		Node(E d, Node n) {
			data = d;
			next = n;
		}
		
		// constructs a Node holding data d and a null reference link
		Node(E d) {
			this(d, null);
		}
	}
	
}