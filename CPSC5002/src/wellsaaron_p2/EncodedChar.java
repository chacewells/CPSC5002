/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_p2;

/**
 * Class representing an encoded char per Assignment P2 Instructions.
 * <p>
 * Note: This class and its API are package private as it will only be used
 * by ListNode and MessageDecoder in this package. The instance fields are also
 * left package-private for convenience and in keeping with the package private
 * field declarations in ListNode.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
class EncodedChar implements Comparable<EncodedChar> {
	// The encoded character to be ordered by index
	char character;
	// The index indicating the order in which character will be unscrambled
	int index;
	
	/**
	 * Constructs a new EncodedChar instance given a character and its position.
	 * Only used by copy-constructor, so private.
	 * 
	 * @param character The character.
	 * @param index The position.
	 */
	private EncodedChar(char character, int index) {
		this.character = character;
		this.index = index;
	}
	
	/**
	 * Copy constructor.
	 * 
	 * @param toCopy The EncodedChar object to copy.
	 */
	EncodedChar(EncodedChar toCopy) {
		this(toCopy.character, toCopy.index);
	}
	
	/**
	 * Convenience factory method for parsing an EncodedChar from a String.
	 * 
	 * @param line A line representing an encoded character.
	 * The line should be in the format "c i" where 'c' is a single ASCII
	 * character and 'i' is a positive (non-zero) integer.
	 * @return A new EncodedChar where character is 'c above and index is 'i'
	 * above.
	 * @throws IllegalArgumentException If the line does not match this method's
	 * format specification.
	 */
	static EncodedChar parseLine(String line) throws IllegalArgumentException {
		validateEncodedLine(line);
		
		char character = extractChar(line);
		int index = extractIndex(line);
		
		return new EncodedChar(character, index);
	}
	
	// Extracts the encoded char from a line
	private static char extractChar(String line) {
		int charPosition = 0;
		return line.charAt(charPosition);
	}
	
	// Extracts the index indicating the proper position of character
	private static int extractIndex(String line) {
		int indexPosition = 2;
		return Integer.parseInt(line.substring(indexPosition).trim());
	}
	
	// Validates the line format according to isLineValid(String) below
	private static void validateEncodedLine(String line) {
		if (!isLineValid(line)) {
			throw new IllegalArgumentException("The line does not match the "
					+ "specified pattern!");
		}
	}

	// Implementing Comparable interface to make ordered insertion more
	// convenient in MessageDecoder.
	@Override
	public int compareTo(EncodedChar o) {
		return Integer.valueOf(index).compareTo(o.index);
	}
	
	/**
	 * Indicates whether the String is in proper encoded character format.
	 * A valid format is an char followed by whitespace followed by
	 * a positive integer.
	 * @param line The line to validate.
	 * @return True if it matches the correct pattern, false otherwise.
	 */
	public static boolean isLineValid(String line) {
		/* Pattern representing a valid input format for line.
		 * Asserts the line can begin with any one character, which must be
		 * followed by a single horizontal whitespace character, then by a
		 * sequence representing a positive integer.
		 * See https://tinyurl.com/pzdjgkq
		 */
		final String validFormatPattern = "^.\\h[1-9]\\d*";
		
		return line.matches(validFormatPattern);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		
		if (obj == this) {
			return true;
		}
		
		if (obj instanceof EncodedChar) {
			EncodedChar other = (EncodedChar) obj;
			if (other.character == character && other.index == index) {
				return true;
			}
		}
		
		return false;
	}

}
