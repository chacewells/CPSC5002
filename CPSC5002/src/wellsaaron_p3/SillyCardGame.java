/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_p3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Plays a silly card game with the user, as explained in the GameModel class's
 * JavaDoc.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class SillyCardGame {

	/**
	 * The main entry point for the SillyCardGame program.
	 * 
	 * @param args No program arguments.
	 */
	public static void main(String[] args) {
		int numberOfPlayers; // The number of players for this card game
		String[] playerNames; // The player names; provided by user
		GameModel game; // The ADT backing this card game
		
		try (
			BufferedReader console = new BufferedReader(
					new InputStreamReader(System.in));
				) {
			do {
				numberOfPlayers = GameModel.DEFAULT_NUMBER_OF_PLAYERS;
				greetUser(numberOfPlayers);
				playerNames = getPlayerNames(numberOfPlayers, console);
				
				game = new GameModel(playerNames);
				game.deal();
				
				do {
					System.out.println(game.getGameState());
					game.playTurn();
					System.out.println(game.getLastTurnInfo() + "\n\n");
				} while (!game.isOver());
				
				System.out.println(game.getWinnerMessage());
			} while (playAgain(console));
			
			System.out.println("Thanks for playing! Goodbye!");
		} catch (IOException e) {
			System.out.println("Error getting console. Aborting!");
			System.exit(1);
		}
	}

	// Welcomes the user and explains the game rules
	private static void greetUser(int numberOfPlayers) {
		StringBuilder builder = new StringBuilder();
		builder
			.append("Welcome to this silly card game!\n")
			.append("In this game, there are " + numberOfPlayers + 
					" players.\n")
			.append("Each player will be dealt 7 cards. On each player's\n"
					+ "turn, their next card will be compared with the top\n"
					+ "card in the deal pile.\n")
			.append("* If your card is HIGHER THAN the deal pile card, you\n"
					+ "  get to discard an additional card from your hand.\n")
			.append("* If your card is EQUAL TO the deal pile card, you\n"
					+ "  must draw one card.\n")
			.append("* If you card is LESS THAN the deal pile card, you\n"
					+ "  must draw two cards.\n")
			.append("First, you'll need to enter the names of your players."
					+ "\n\n");
		
		System.out.println(builder.toString());
	}

	// Prompts the user for each player's name
	private static String[] getPlayerNames(
			int numberOfPlayers,
			BufferedReader console) {
		final String[] playerNames = new String[numberOfPlayers];
		for (int currentPlayer = 1; 
				currentPlayer <= playerNames.length;
				++currentPlayer) {
			getPlayerNameFromUser(playerNames, currentPlayer, console);
		}
		
		return playerNames;
	}

	// Prompts the user for a player name and adds it to the playerNames array
	private static void getPlayerNameFromUser(
			String[] playerNames,
			int currentPlayer,
			BufferedReader console) {
		int playerIndex = currentPlayer - 1;
		String playerNameFromUser;
		
		try {
			System.out.print("Please enter a name for player " + 
					currentPlayer + ": ");
			playerNameFromUser = console.readLine();
			
			playerNames[playerIndex] = playerNameFromUser.trim();
		} catch (IOException e) {
			playerNames[playerIndex] = "Player " + currentPlayer;
			
			System.out.println("Error reading input from user. "
					+ "Defaulting to " + playerNames[playerIndex]);
		}
	}
	
	// Asks the user if they'd like another game and returns true on "y", false
	// otherwise
	private static boolean playAgain(BufferedReader console) {
		System.out.print("Would you like another game (y/n)? ");
		try {
			String userAnswer = console.readLine();
			if ("y".equalsIgnoreCase(userAnswer.trim())) {
				return true;
			} else {
				return false;
			}
		} catch (IOException e) {
			System.out.println("Error reading user input. Aborting!");
			return false;
		}
	}

}
