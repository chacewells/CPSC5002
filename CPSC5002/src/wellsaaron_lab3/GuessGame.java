/*
 * 
 */
package wellsaaron_lab3;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Random;

/**
 * Interfaces for playing a guessing game.
 * 
 * @author wellsaaron
 *
 */
public class GuessGame {
	private int rangeMinimum;
	private int rangeMaximum;
	private int range;
	private int guessesAttempted;
	private int target;

	public GuessGame(int rangeMinimum, int rangeMaximum) {
		this.guessesAttempted = 0;
		this.rangeMinimum = rangeMinimum;
		this.rangeMaximum = rangeMaximum;
		this.range = this.rangeMaximum - this.rangeMinimum;
		
		this.newTarget();
	}
	
	public void playGame(BufferedReader console) {
		while (true) {
			try {
				String userInput = getInputFromUser(console,
						String.format("Guess a number between %d and %d: ", 
								rangeMinimum, rangeMaximum));
				Integer guess = Integer.parseInt(userInput);
				
				if (guess(guess)) {
					System.out.println("That's correct!");
					return;
				}
			} catch (IOException|NumberFormatException e) {
				printValidNumberMessage();
			}
		}
	}
	
	private void printValidNumberMessage() {
		System.out.println("Please enter a valid integer!");
	}
	
	private static String getInputFromUser(
			BufferedReader console, 
			String request) throws IOException {
		System.out.print(request);
		String userInput = console.readLine();
		
		return userInput;
	}

	/**
	 * Displays number of guesses.
	 */
	public void displayStatistics() {
		System.out.println("You guessed " + this.guessesAttempted + " times.");
	}

	/**
	 * Randomly chooses a new integer in this game's range which is now the new
	 * target.
	 */
	public void newTarget() {
		int rangeExclusive = range - 1;
		Random rand = new Random();
		this.target = rand.nextInt(rangeExclusive) + rangeMinimum;
	}

	/**
	 * Processes the user's guess, returns false if number is incorrect; otherwise,
	 * returns true.
	 * 
	 * @param num The user's guess
	 * @return Whether the guess is correct
	 */
	public boolean guess(int num) {
		boolean isCorrect = num == this.target;
		if (!isCorrect) {
			displayHint(num);
		}

		++this.guessesAttempted;

		return isCorrect;
	}

	/**
	 * returns the low end of the range used by newTarget
	 * 
	 * @return The value of RANGE_MINIMUM
	 */
	public int getRangeMinimum() {
		return this.rangeMinimum;
	}

	/**
	 * returns the high end of the range used by newTarget
	 * 
	 * @return The value of RANGE_MAXIMUM
	 */
	public int getRangeMaximum() {
		return this.rangeMaximum;
	}

	/**
	 * Called from guess function; displays whether the guess was too high or too
	 * low.
	 * 
	 * @param num
	 */
	private void displayHint(int num) {
		String direction = num < this.target ? "low" : "high";
		System.out.println(num + " is too " + direction + ".");
	}

}
