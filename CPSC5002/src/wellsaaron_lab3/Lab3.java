/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_lab3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Plays a guessing game with the user.
 * @author wellsaaron
 *
 */
public class Lab3 {

	private static final String GREETING = "Welcome to the Guessing Game " +
			"program! This is a guessing game where you will \n" + 
			"guess a number and I tell you if it is too low or too high.\n";
	
	private static final int GUESS_MINIMUM = 15_000;
	private static final int GUESS_MAXIMUM = 25_000;

	public static void main(String[] args) throws IOException {
		try (BufferedReader console = new BufferedReader(new InputStreamReader(System.in))) {
			System.out.println(GREETING);
			
			while (true) {
				GuessGame game = new GuessGame(GUESS_MINIMUM, GUESS_MAXIMUM);
				game.playGame(console);
				game.displayStatistics();
				
				if (declinePlayAgain(console)) {
					break;
				}
			}

			System.out.println("Thanks for using the Guessing Game program!");
		}
		
	}
	
	private static boolean declinePlayAgain(BufferedReader console) throws IOException {
		System.out.print("Ready to play again? (no to quit) ");
		String userInput = console.readLine();
		return userInput.matches("no");
	}

}
