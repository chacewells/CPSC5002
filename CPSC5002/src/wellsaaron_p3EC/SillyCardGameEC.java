/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_p3EC;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Plays a silly card game with the user, as explained in the GameModel class's
 * JavaDoc.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class SillyCardGameEC {

	/**
	 * The main entry point for the SillyCardGame program.
	 * 
	 * @param args No program arguments.
	 */
	public static void main(String[] args) {
		int numberOfPlayers; // The number of players for this card game
		String[] playerNames; // The player names; provided by user
		GameModelEC game; // The ADT backing this card game
		
		try ( // try w/ resources: see https://tinyurl.com/nggbv2b
			BufferedReader console = new BufferedReader(
					new InputStreamReader(System.in));
				) {
			do { // outer loop; play again?
				greetUser();
				numberOfPlayers = getNumberOfPlayersFromUser(console);
				playerNames = getPlayerNames(numberOfPlayers, console);
				
				game = new GameModelEC(playerNames);
				game.deal();
				
				do { // inner loop; play the game
					System.out.println(game.getGameState());
					game.playTurn();
					System.out.println(game.getLastTurnInfo() + "\n\n");
				} while (!game.isOver());
				
				System.out.println(game.getWinnerMessage());
			} while (playAgain(console));
			
			System.out.println("Thanks for playing! Goodbye!");
		} catch (IOException e) {
			System.out.println("Error getting console. Aborting!");
			System.exit(1);
		}
	}

	// Welcomes the user and explains the game rules
	private static void greetUser() {
		StringBuilder builder = new StringBuilder();
		builder
			.append("Welcome to this silly card game!\n")
			.append("In this game, there are two to six players.\n")
			.append("Each player will be dealt 7 cards. On each player's\n"
					+ "turn, their next card will be compared with the top\n"
					+ "card in the deal pile.\n")
			.append("* If your card is HIGHER THAN the deal pile card, you\n"
					+ "  get to discard an additional card from your hand.\n")
			.append("* If your card is EQUAL TO the deal pile card, you\n"
					+ "  must draw one card.\n")
			.append("* If you card is LESS THAN the deal pile card, you\n"
					+ "  must draw two cards.\n")
			.append("First, you'll need to choose the number of players.\n")
			.append("Then, you'll need to enter the players' names.\n");
		
		System.out.println(builder.toString());
	}
	
	// gets the number of players from the user
	private static int getNumberOfPlayersFromUser(BufferedReader console) {
		final int minNumberOfPlayers = 2, maxNumberOfPlayers = 6;
		final String validNumberPattern = "^\\d$";
		String userInput = null;
		int numberOfPlayers = minNumberOfPlayers;
		
		do {
			try {
				System.out.print("How many will play (between 2 and 6)? ");
				userInput = console.readLine();
				
				if (!userInput.matches(validNumberPattern)) {
					System.out.println("Please enter a valid integer.");
					continue;
				}
				
				numberOfPlayers = Integer.parseInt(userInput);
			} catch (IOException e) {
				System.out.println("Error reading user input.\n"
						+ "Defaulting to two players.");
				numberOfPlayers = minNumberOfPlayers;
			}
		} while (numberOfPlayers < minNumberOfPlayers
				|| numberOfPlayers > maxNumberOfPlayers);
			
		return numberOfPlayers;
	}

	// Prompts the user for each player's name
	private static String[] getPlayerNames(
			int numberOfPlayers,
			BufferedReader console) {
		final String[] playerNames = new String[numberOfPlayers];
		for (int currentPlayer = 1; 
				currentPlayer <= playerNames.length;
				++currentPlayer) {
			getPlayerNameFromUser(playerNames, currentPlayer, console);
		}
		
		return playerNames;
	}

	// Prompts the user for a player name and adds it to the playerNames array
	private static void getPlayerNameFromUser(
			String[] playerNames,
			int currentPlayer,
			BufferedReader console) {
		int playerIndex = currentPlayer - 1;
		String playerNameFromUser;
		
		try {
			System.out.print("Please enter a name for player " + 
					currentPlayer + ": ");
			playerNameFromUser = console.readLine();
			
			playerNames[playerIndex] = playerNameFromUser.trim();
		} catch (IOException e) {
			playerNames[playerIndex] = "Player " + currentPlayer;
			
			System.out.println("Error reading input from user. "
					+ "Defaulting to " + playerNames[playerIndex]);
		}
	}
	
	// Asks the user if they'd like another game and returns true on "y", false
	// otherwise
	private static boolean playAgain(BufferedReader console) {
		System.out.print("Would you like another game (y/n)? ");
		try {
			String userAnswer = console.readLine();
			if ("y".equalsIgnoreCase(userAnswer.trim())) {
				return true;
			} else {
				return false;
			}
		} catch (IOException e) {
			System.out.println("Error reading user input. Aborting!");
			return false;
		}
	}

}
