/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_p3EC;

import java.util.EmptyStackException;

/**
 * Implements a queue backed by a linked list.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class Queue {
	// the front of the queue
	private Node front;
	// the rear of the queue
	private Node rear;
	private int size;
	
	public Queue() {
		size = 0;
	}
	
	/**
	 * Adds an element to the rear of this queue.
	 * 
	 * @param data The integer to add.
	 */
	public void enqueue(Integer data) {
		if (empty()) {
			rear = new Node(data);
			front = rear;
		} else {
			rear.next = new Node(data);
			rear = rear.next;
		}
		
		++size;
	}
	
	/**
	 * Removes and returns the element at the front of this queue.
	 * 
	 * @return
	 */
	public Integer dequeue() {
		if (empty()) {
			throw new EmptyStackException();
		}
		
		Integer value = front.data;
		front = front.next;
		if (front == null) {
			rear = null;
		}
		
		--size;
		
		return value;
	}
	
	/**
	 * Indicates whether the list is empty.
	 * 
	 * @return True if the list is empty; false otherwise.
	 */
	public boolean empty() {
		return rear == null;
	}
	
	/**
	 * Returns the value at the front of the queue, without removing it.
	 * 
	 * @return The value at the front of the queue.
	 */
	public Integer peek() {
		if (empty()) {
			throw new EmptyStackException();
		}

		return front.data;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder("[");
		Node current = front;
		while (current != null) {
			builder
				.append(String.format("%2d", current.data))
				.append(current.next != null ? "," : "");
			
			current = current.next;
		}
		builder.append("]");
		
		return builder.toString();
	}
	
	/**
	 * The number of elements in the queue.
	 * 
	 * @return The size of the queue.
	 */
	public int size() {
		return size;
	}
	
	/**
	 * A node representing an element in the queue.
	 *
	 * @author  Aaron Wells
	 * @version 1.0
	 */
	private static class Node {
		// The data to hold
		Integer data;
		// Link to the next element in the chain
		Node next;
		
		// constructs a Node holding data d and a reference to link n
		Node(Integer d, Node n) {
			data = d;
			next = n;
		}
		
		// constructs a Node holding data d and a null reference link
		Node(Integer d) {
			this(d, null);
		}
	}
	
}