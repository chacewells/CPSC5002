/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_p3EC;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.function.IntFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Model class for a simple card game.<br>
 * <br>
 * <p>The game is played as follows:</p>
 *
 *	<ol>
 *	    <li>The cards are shuffled and placed into a stack</li>
 *	    <li>Each player is dealt 7 cards from the stack in a round-robin fashion
 *	     and their cards are placed into their queue</li>
 *	    <li>The next card in the deal stack is placed into the discard stack
 *	    	</li>
 *	    <li>For their turn, each player plays the next card in his/her queue.
 *	   		</li>
 *	        <ul>
 *	            <li>
 *		            If the card the player plays is HIGHER in number than the 
 *		            one on the top of the discard stack, the player's turn is 
 *		            over.
 *	            </li>
 *	            <li>
 *	            	If the card the player plays is EQUAL in number to the one 
 *	            	on the top of the discard stack, the player must then take 
 *	            	one card from the deal stack and the player's turn is over.
 *            	</li>
 *	            <li>
 *		            If the player's card is LOWER in number than the one on the 
 *		            discard stack, the player must take TWO cards from the deal 
 *		            stack and the player's turn is over.
 *	            </li>
 *	            <li>
 *		            If the deal stack runs out of cards, "turn over" the discard 
 *		            stack and continue the game.
 *	            </li>
 *	        </ul>
 *	    <li>The first player to run out of cards wins the game.</li>
 *	    <li>Offer to repeat the game as many times as desired.</li>
 *	</ol>
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class GameModelEC {
	// constants for game rules
	
	// cards per suit ("suit" is kind of meaningless here; more like, maximum
	// value that can exist per card
	public static final int CARDS_IN_SUIT = 13;
	public static final int CARDS_IN_DECK = CARDS_IN_SUIT * 4; // cards in deck
	public static final int INITIAL_CARDS_PER_HAND = 7; // cards to deal
	public static final int DEFAULT_NUMBER_OF_PLAYERS = 2; // # players
	
	/**
	 * Enum type for cards drawn. Holds the number of cards and offers a bit
	 * of convenience and readability when determining how many cards the user
	 * will draw after a round of play.
	 * 
	 * @author wellsaaron
	 * @version 1.0
	 *
	 */
	public enum CardsDrawn {
		/**
		 * take 2 if user's card value is less than the deal pile's
		 */
		LESS_THAN_DISCARD(2),
		/**
		 * take 1 if user's card value is equal to the deal pile's
		 */
		MATCHES_DISCARD(1),
		/**
		 * take none if user's card value is greater than the deal pile's
		 */
		GREATER_THAN_DISCARD(0);
		
		private final int cards; // number of cards to draws
		
		// private constructor specifying the # of cards to take
		private CardsDrawn(int cards) {
			this.cards = cards;
		}
	
		/**
		 * Returns number of cards to draw
		 * @return The number of cards to draw.
		 */
		public int getCards() {
			return cards;
		}
		
		/**
		 * Simply returns the String value of cards
		 */
		@Override
		public String toString() {
			return String.valueOf(cards);
		}
		
		/**
		 * Maps integer comparison of player's card value and the topmost
		 * discard card to a CardsDrawn enum.
		 * 
		 * @param playerCard The current player's card value.
		 * @param discardCard The last discarded deal pile card value.
		 * @return If playerCard > discardCard, GREATER_THAN_DISCARD; if 
		 * playerCard == discardCard, MATCHES_DISCARD; otherwise 
		 * LESS_THAN_DISCARD.
		 */
		public static CardsDrawn determine(
				Integer playerCard,
				Integer discardCard) {
			if (playerCard > discardCard) {
				return GREATER_THAN_DISCARD;
			} else if (playerCard == discardCard) {
				return MATCHES_DISCARD;
			} else {
				return LESS_THAN_DISCARD;
			}
		}
	}
	
	// the deck
	private Stack dealPile;
	// the discard pile
	private Stack discardPile;

	// the player hands
	private Queue[] playerHands;
	
	// the player names
	private String[] playerNames;
	
	// the current player
	private int currentPlayer;
	
	// number of players
	private final int numberOfPlayers;
	
	// Game state
	private StringBuilder gameState;
	
	// information about the last turn taken
	private StringBuilder lastTurnInfo;
	
	// The winner
	private Integer winner;

	/**
	 * Starts a new game and initializes the game state. Does NOT deal the
	 * cards, so client code must call deal() to begin the game.
	 * 
	 * @param playerNames An array of strings representing the players in the 
	 * game.
	 */
	public GameModelEC(String[] playerNames) {
		dealPile = new Stack(CARDS_IN_DECK);
		discardPile = new Stack(CARDS_IN_DECK);
		numberOfPlayers = playerNames.length;
		this.playerNames = Arrays.copyOf(playerNames, numberOfPlayers);
		
		initializePlayerHands();
		currentPlayer = 0;
	}
	
	/**
	 * Plays a turn of the game, which involves:
	 * 1. Turning over and discarding the deal pile's top card
	 * 2. Comparing the last discarded card with the player's card
	 * 3. Distributing cards according to the game rules
	 * 4. Checking if the current player has won (run out of cards in hand)
	 * 5. Updating the game state for display by client code
	 */
	public void playTurn() {
		Integer playerCard,    // current player's next card 
		discardCard;           // the top of the discard pile
		CardsDrawn cardsDrawn; // the number of cards the player takes
		
		lastTurnInfo = new StringBuilder()
				.append(playerNames[currentPlayer] + " takes a turn.\n");
		
		playerCard = playerHands[currentPlayer].dequeue();
		discardCard = discardPile.peek();
		/*
		 *  This ensures there are always at least 2 cards in the discard pile:
		 *  the last player's card, and the card this player played. This
		 *  prevents EmptyStackExceptions in the for loop below, and this is
		 *  probably a result of the rules anyway.
		 */
		discardPile.push(playerCard);
		cardsDrawn = CardsDrawn.determine(playerCard, discardCard);
		
		if (cardsDrawn == CardsDrawn.GREATER_THAN_DISCARD) {
			lastTurnInfo.append("Your card " + playerCard + 
					" is greater than " + discardCard + ".\n")
			.append("Good job!");
		} else if (cardsDrawn == CardsDrawn.LESS_THAN_DISCARD) {
			lastTurnInfo.append("Your card " + playerCard +
					" is less than " + discardCard + ".\n")
			.append("You pick " + cardsDrawn + " cards.");
		} else {
			lastTurnInfo.append("Your card " + playerCard + 
					" is equal to " + discardCard + ".\n")
			.append("You pick " + cardsDrawn + " card.");
		}
		
		// block won't execute if cardsDrawn == GREATER_THAN_DISCARD
		for (int i = 0; i < cardsDrawn.getCards(); ++i) {
			refreshDealPile();
			dealCard(currentPlayer);
		}
		
		checkForWinner();
		
		nextPlayer();
		buildGameState();
	}

	/**
	 * A string representation of the game's current state.
	 * 
	 * @return A human-readable description of the game's current state.
	 */
	public String getGameState() {
		return gameState.toString();
	}

	/**
	 * Deal every player a hand.
	 */
	public void deal() {
		initializeDealPile();
		
		for (int i = 0; i < INITIAL_CARDS_PER_HAND; ++i) {
			dealOneAllPlayers();
		}
		
		initializeDiscardPile();
		buildGameState();
	}

	/**
	 * @return A congratulation message to the winner.
	 */
	public String getWinnerMessage() {
		if (!isOver()) { // this shouldn't happen
			throw new IllegalStateException("The game's not over yet!");
		}
		
		return "Congratulations, " + playerNames[winner] + " you win!";
	}

	/**
	 * Whether the game is over.
	 * 
	 * @return True if there is a winner; false otherwise.
	 */
	public boolean isOver() {
		return winner != null || (dealPile.empty() && discardPile.empty());
	}

	/**
	 * @return A human-readable message describing the last turn.
	 */
	public String getLastTurnInfo() {
		return lastTurnInfo.toString();
	}

	// Allocates memory for the Queue objects representing the player hands
	private void initializePlayerHands() {
		playerHands = new Queue[numberOfPlayers];
		for (int i = 0; i < numberOfPlayers; ++i) {
			playerHands[i] = new Queue();
		}
	}
	
	// Initializes the deal pile with a shuffled deck
	private void initializeDealPile() {
		// An unshuffled deck of cards
		ArrayList<Integer> deck = getUnshuffledDeck();
		shuffleDeck(deck);
		
		for (Integer card : deck) {
			dealPile.push(card);
		}
	}
	
	// Initializes the discard pile by flipping over a card from the deal pile
	private void initializeDiscardPile() {
		flipDealCard();
	}
	
	// Sets the winner to the current player if his/her hand is empty
	private void checkForWinner() {
		if (playerHands[currentPlayer].empty()) {
			winner = currentPlayer;
		}
	}
	
	// Fills the deal pile with the contents of the discard pile
	private void turnOverDiscardPile() {
		Integer card; // The next card in the discard pile
		Integer top = discardPile.pop(); // hold onto top card
		
		while (!discardPile.empty()) {
			card = discardPile.pop();
			dealPile.push(card);
		}
		
		discardPile.push(top);
	}
	
	// If the deal pile is empty, refill it with the contents of the discard
	// pile
	private void refreshDealPile() {
		if (dealPile.empty()) {
			turnOverDiscardPile();
		}
	}
	
	// Rotates players
	private void nextPlayer() {
		currentPlayer++;
		currentPlayer = currentPlayer % numberOfPlayers;
	}
	
	// Transfers the top card on the deal pile to the discard pile
	private void flipDealCard() {
		// the card to flip
		Integer card = dealPile.pop();
		discardPile.push(card);
	}
	
	// Builds a character sequence representing the current game state;
	// Stores it in this.gameState
	private void buildGameState() {
		gameState = new StringBuilder("Cards in hand\n")
							  .append("-------------\n");
		for (int i = 0; i < numberOfPlayers; ++i) {
			gameState.append(getPlayerHandView(i) + "\n\n");
		}
		
		gameState.append("Top of the stack is " + discardPile.peek() + "\n\n");
	}

	// The player name followed by their hand on a new line
	private String getPlayerHandView(int playerNumber) {
		// the character buffer
		StringBuilder builder = new StringBuilder(
				playerNames[playerNumber] + "\n");
		builder.append(playerHands[playerNumber]);
		
		return builder.toString();
	}

	// Deals a single card to the given player
	private void dealCard(int playerNumber) {
		Integer card; // the card to deal
		
		if (dealPile.empty()) { // deal pile empty, even after refilling
			return;
		}
		card = dealPile.pop();
		playerHands[playerNumber].enqueue(card);
	}
	
	// Deals one card to each player
	private void dealOneAllPlayers() {
		for (int i = 0; i < numberOfPlayers; ++i) {
			dealCard(i);
		}
	}
	
	// Get an unshuffled deck of cards as an ArrayList<Integer>
	// Using functional API for succinctness: https://tinyurl.com/y8v82u6q
	private ArrayList<Integer> getUnshuffledDeck() {
		// Generates a stream of four of the given integer
		IntFunction<IntStream> fourTimes = i -> IntStream.of(i, i, i, i);
		
		// The deck
		List<Integer> deck = IntStream
				.range(1, CARDS_IN_SUIT + 1)
				.flatMap(fourTimes)
				.mapToObj(Integer::valueOf)
				.collect(Collectors.toList());
		
		return new ArrayList<>(deck);
	}
	
	/**
	 * Shuffles the cards using the
	 * <a href="https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle">
	 * Fisher-Yates algorithm</a>
	 * @param cards deck to shuffle
	 */
	private void shuffleDeck(ArrayList<Integer> cards) {
	    Random rand = new Random();
	    for (int i = cards.size(); i > 1; i--) {
	        int j = rand.nextInt(i);
	        int temp = cards.get(i - 1);
	        cards.set(i - 1, cards.get(j));
	        cards.set(j, temp);
	    }
	}

}
