/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_lab2;

import java.util.Random;

/**
 * Wrapper class of Die that weights the chances of a particular side
 * being rolled.
 * @author wellsaaron
 *
 */
public class LoadedDie {
	public static final int SIDES = 6;
	private Die die;
	private int value;
	private int loadedNumber;
	private int moreTimesPerHundred;
	
	 /**
	 * The constructor performs an initial roll of the die.
	 * @param loadedNumber        which number should come up more often
	 * @param moreTimesPerHundred how many times per 100 rolls to come up with 
	 *                            the loaded number (instead of uniform random)
	 */
	public LoadedDie(int loadedNumber, int moreTimesPerHundred) {
		this.loadedNumber = loadedNumber;
		this.moreTimesPerHundred = moreTimesPerHundred;
		this.die = new Die(SIDES);
		this.roll();
	}
	
	/**
	 * Accessor for the die's value.
	 * @return The die's value.
	 */
	public int getValue() {
		return value;
	}

	/**
	* The roll method simulates the rolling of the die.
	* It will typically set this die's value to a random value
	* with uniform distribution between 1 and 6. Occasionally,
	* it will a priori return the favored value (with frequency
	* determined by the moreTimesPerHundred argument that was passed
	* to the constructor).
	*/
	public void roll() {
		Random random = new Random();
		int weight = random.nextInt(100);
		
		if (weight < this.moreTimesPerHundred) {
			this.value = this.loadedNumber;
		} else {
			this.die.roll();
			this.value = this.die.getValue();
		}
	}
	
}
