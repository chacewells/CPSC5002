/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_lab2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Plays a dice game with the user.
 * 
 * @author wellsaaron
 *
 */
public class Lab2 {

	public static void main(String[] args) throws IOException {
		
		while (true) {
			DiceGame diceGame = new DiceGame();
			
			diceGame.greetUser();
			diceGame.play();
			
			if (!diceGame.playAgain()) {
				break;
			}
		}
		
	}

	/**
	 * Helper class provides an interface to a dice game. Greets the user, starts a game, keeps score, and asks 
	 * the user if he/she would like to play again.
	 *  
	 * @author wellsaaron
	 *
	 */
	private static class DiceGame {
		public static final String GREETING = "Welcome to the Dice Game program! This is a game of you versus the \n" + 
				"computer. We will each have one die. We roll our own die and the \n" + 
				"higher number wins. We roll ten times and the one with the higher \n" + 
				"number of wins is the grand winner.\n";
		
		private int computerScore = 0;
		private int userScore = 0;
		
		/* 
		 * TODO: This is bad, but if I use try w/ resources, System.in is closed with the reader,
		 * so the next read throws an exception and ends the program. Advice welcome :)
		 */
		private BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
		
		/**
		 * Greets the user.
		 */
		public void greetUser() {
			System.out.println(GREETING);
		}
		
		/**
		 * Plays the game. Rolls the computer's and the user's dice ten times each, keeping score for each.
		 * The scores are tallied and a winner is declared upon completion.
		 */
		public void play() {
			LoadedDie computerDie = new LoadedDie(6, 30);
			LoadedDie userDie = new LoadedDie(1, 30);
			
			for (int rollNumber = 1; rollNumber <= 10; ++rollNumber) {
				System.out.println("Roll " + rollNumber + " of 10:");
				computerDie.roll();
				System.out.println("I rolled a " + computerDie.getValue());
				System.out.print("Ready to roll? (Press ENTER when ready)");
				this.waitForUser();
				
				userDie.roll();
				System.out.println("You rolled a " + userDie.getValue());
				
				if (computerDie.getValue() > userDie.getValue()) {
					this.computerScore++;
				} else if (userDie.getValue() > computerDie.getValue()) {
					this.userScore++;
				} else {
					System.out.println("It's a tie! Let's redo this round.");
					System.out.println();
					continue;
				}
				System.out.println();
			}
			
			this.declareWinner();
		}
		
		private void waitForUser() {
			try {
				console.readLine(); // block until user presses ENTER
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			}
		}
		
		private void declareWinner() {
			String winnerTemplate = "Grand winner is %s!%n";
			
			System.out.println("I won " + this.computerScore + " times.");
			System.out.println("You won " + this.userScore + " times.");
			
			if (this.computerScore > this.userScore) {
				System.out.printf(winnerTemplate, "me");
			} else if (this.userScore > this.computerScore) {
				System.out.printf(winnerTemplate, "you");
			} else {
				System.out.println("It was a tie!");
			}
		}
		
		/**
		 * Asks the user if he/she would like to play again.
		 * 
		 * @return If the user's answer contains "no", false; otherwise true.
		 */
		public boolean playAgain() {
			boolean playAgain = true;
			
			try {
				System.out.print("Ready to play? (no to quit) ");
				String userInput = console.readLine();
				
				if (userInput.matches("no")) {
					playAgain = false;
				}
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			}
			
			return playAgain;
		}
		
	}
	
}
