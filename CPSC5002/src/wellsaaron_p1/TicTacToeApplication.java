/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_p1;
 
import java.util.stream.IntStream;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/*
 * Contains the main() method.
 * Should include the ability to repeat the game.
 * All of user input from the keyboard must be performed in this class
 * Uses the public methods of your TicTacToe class to test functionality (i.e. play the game).
 * Be sure to create functions that do one task and only one task.
 */

/**
 * 
 * @author wellsaaron
 *
 */
public class TicTacToeApplication extends Application {
	
	private TicTacToe ticTacToe;
	
	private void updateTile(Button btn, int row, int col) {
		String markString = String.valueOf(this.ticTacToe.get(row, col));
		btn.setText(markString);
	}
	
	@Override
	public void init() throws Exception {
    	this.ticTacToe = new TicTacToe(3);
	}
	
	private GridPane buildRoot() {
		GridPane root = new GridPane();
    	
        IntStream.range(0, 9).forEach(i -> {
        	int col = i % 3;
        	int gridCol = col + 1;
        	int row = i / 3;
        	
        	Button btn = new Button();
        	updateTile(btn, row, col);
        	
        	btn.setOnAction((ActionEvent event) -> {
        		this.ticTacToe.playTile(row, col);
            	updateTile(btn, row, col);
			});
        	
        	root.add(btn, gridCol, row);
        });
        
        return root;
	}
	
    @Override
    public void start(Stage primaryStage) {
    	GridPane root = buildRoot();

        Scene scene = new Scene(root, 300, 250);

        primaryStage.setTitle("Tic Tac Toe");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    
	public static void main(String[] args) {
		 launch();
	}
}