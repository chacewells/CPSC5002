package wellsaaron_p1;

public enum Mark {
	X("X"), O("O"), BLANK(" ");
	
	private String stringValue;
	
	private Mark(String stringValue) {
		this.stringValue = stringValue;
	}
	
	@Override
	public String toString() {
		return this.stringValue;
	}
}
