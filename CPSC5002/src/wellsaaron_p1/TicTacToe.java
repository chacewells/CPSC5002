/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_p1;

import java.util.Arrays;

/*
 * The board, with the actions that can be taken on it, will be in a class.
 * 
 * The class should be responsible for displaying statistics, displaying the 
 * board, taking turns between players, and finding a winner. 
 * 
 * See comments for additional functionality that may be included in the 
 * TicTacToe class.
 * 
 * TicTacToe class does not call any methods of the P1 class.
 * 
 * Be sure to create functions that do one task and only one task.
 */

/**
 * The TicTacToe game board. 
 * @author aaron
 *
 */
public class TicTacToe {
	private Mark[][] board;
	private int boardWidth;
	private Mark currentPlayer;
	private Mark winner;
	
	// game statistics
	private int xWins = 0, oWins = 0, tieGames = 0;

	/**
	 * Constructs a new TicTacToe board. Fails to initialize if the board width 
	 * is even or the board is less than 3 tiles wide.
	 * 
	 * @param boardWidth The board width, in tiles.
	 * @throws IllegalArgumentException if the the board width is even or less 
	 * than 3 tiles.
	 */
	public TicTacToe(int boardWidth) {
		this.boardWidth = boardWidth;
		validateBoardWidth();
		initializeBoard();
		initializePlayer();
	}
	
	/**
	 * Plays the specified tile and advances the game to the alternate player.
	 * @param row The tile's row.
	 * @param col The tile's column.
	 */
	public void playTile(int row, int col) {
		this.board[row][col] = this.currentPlayer;
		checkForWinner(row, col);
		if (isGameOver()) {
			incrementWinner();
		}
		switchPlayers();
	}

	/**
	 * Resets the board for a new game.
	 */
	public void reset() {
		initializeBoard();
		initializePlayer();
		this.winner = null;
	}
	
	private void initializePlayer() {
		this.currentPlayer = Mark.X; // X always goes first
	}
	
	/**
	 * Checks the row, column, and diagonals (if applicable) for the given tile
	 * position. If a winner is found, the winner field is updated with the
	 * value found at the given position.
	 * 
	 * @param row The tile position's row coordinate.
	 * @param col The tile position's column coordinate.
	 */
	public void checkForWinner(int row, int col) {
		Mark player = this.board[row][col];
		if (player == Mark.BLANK) { // this should never happen
			return;
		}
		
		// check row & col
		if (didPlayerWinAtRow(player, row)
				|| didPlayerWinAtColumn(player, col)) {
			this.winner = player;
			return;
		}
		
		// check diagonals
		if (isOnDiagonal(row, col)
				&& didPlayerWinAtMajorDiagonal(player) 
				|| didPlayerWinAtMinorDiagonal(player)) {
			this.winner = player;
		}
		
	}
	
	/**
	 * Indicates whether the game has ended.
	 * @return If the winner contains a value or all tiles have been played,
	 * then true. Otherwise false.
	 */
	public boolean isGameOver() {
		return this.winner != null || areTilesFull();
	}
	
	private boolean areTilesFull() {
		for (int row = 0; row < this.boardWidth; ++row) {
			for (int col = 0; col < this.boardWidth; ++col) {
				if (this.board[row][col] == Mark.BLANK) {
					return false;
				}
			}
		}
		
		return true;
	}

	/**
	 * Indicates whether the specified tile can be played.
	 * @param row The tile's row.
	 * @param col The tile's column.
	 * @return True if the tile is blank; otherwise false.
	 */
	public boolean canPlayTile(int row, int col) {
		return this.board[row][col] == Mark.BLANK;
	}
	
	/**
	 * Displays a message indicating whose turn it is.
	 */
	public void printTurn() {
		System.out.println(this.currentPlayer + ", it is your turn.");
	}
	
	/**
	 * Declares the winner, shows the final board state, and displays statistics
	 */
	public void displayGameEndMessage() {
		displayWinner();
		displayBoard();
		displayStatistics();
	}

	/**
	 * Displays the board in a pretty-printed format.
	 */
	public void displayBoard() {
		System.out.println('\n');
		System.out.print(stringifyBoard());
	}
	
	private void displayWinner() {
		if (this.winner == null) {
			System.out.println("No winner - it was a tie!");
		} else {
			System.out.printf("The winner is %s!%n", this.winner);
		}
	}
	
	private void displayStatistics() {
		String winsFormat = "%s has won %d games.%n";
		
		System.out.printf(winsFormat, Mark.X, this.xWins);
		System.out.printf(winsFormat, Mark.O, this.oWins);
		System.out.printf("There have been %d tie games.%n", this.tieGames);
	}
	
	private String stringifyBoard() {
		StringBuilder buff = new StringBuilder();
		
		buff.append(getColumnHeader()).append('\n');
		for (int i = 0; i < this.boardWidth; ++i) {
			buff.append(rowToString(i)).append('\n');
			buff.append(getDividingLine()).append('\n');
		}
		
		return buff.toString();
	}
	
	private String getDividingLine() {
		int lastRow = this.boardWidth - 1;
		int spaceForRow = String.valueOf(lastRow).length() + 1;
		int displayWidth = spaceForRow + this.boardWidth * 2;
		
		return repeatChar('-', displayWidth);
	}
	
	private static String repeatChar(char c, int times) {
		char[] charBuffer = new char[times];
		Arrays.fill(charBuffer, c);
		return new String(charBuffer);
	}
	
	private void incrementWinner() {
		if (this.winner == Mark.X) {
			++this.xWins;
		} else if (this.winner == Mark.O) {
			++this.oWins;
		} else {
			++this.tieGames;
		}
	}
	
	private String getColumnHeader() {
		String delimiter = " ";
		StringBuilder buff = new StringBuilder(delimiter).append(delimiter);
		for (int col = 0; col < this.boardWidth; ++col) {
			buff.append("" + col).append(delimiter);
		}
		
		return buff.toString();
	}
	
	private String rowToString(int row) {
		String delimiter = "|", 
				space = " ";
		
		StringBuilder buff = new StringBuilder("" + row).append(space);
		for (int col = 0; col < this.boardWidth; ++col) {
			buff.append(this.board[row][col]).append(delimiter);
		}
		
		return buff.toString();
	}
	
	private void switchPlayers() {
		this.currentPlayer = this.currentPlayer == Mark.X ? Mark.O : Mark.X;
	}
	
	private void initializeBoard() {
		this.board = new Mark[this.boardWidth][this.boardWidth];
		
		for (int row = 0; row < this.boardWidth; ++row) {
			Arrays.fill(board[row], Mark.BLANK);
		}
	}
	
	private void validateBoardWidth() {
		validateBoardWidthIsOddNumber();
		validateBoardWidthAtLeastThree();
	}
	
	private void validateBoardWidthIsOddNumber() {
		String evenBoardWidthNotAllowed = "The board width cannot be even!";
		if (this.boardWidth % 2 == 0) {
			throw new IllegalArgumentException(evenBoardWidthNotAllowed);
		}
	}
	
	private void validateBoardWidthAtLeastThree() {
		int boardWidthMin = 3;
		String widthMinMessage = "The board width must be a minimum of 3!";
		
		if (this.boardWidth < boardWidthMin) {
			throw new IllegalArgumentException(widthMinMessage);
		}
	}
	
	private boolean didPlayerWinAtRow(Mark player, int row) {
		for (int col = 0; col < this.boardWidth; ++col) {
			if (player != this.board[row][col]) {
				return false;
			}
		}
		
		return true;
	}
	
	private boolean didPlayerWinAtColumn(Mark player, int col) {
		for (int row = 0; row < this.boardWidth; ++row) {
			if (player != this.board[row][col]) {
				return false;
			}
		}

		return true;
	}
	
	private static boolean isOnDiagonal(int row, int col) {
		return row == col;
	}
	
	private boolean didPlayerWinAtMajorDiagonal(Mark player) {
		for (int i = 0; i < this.boardWidth; ++i) {
			if (this.board[i][i] != player) {
				return false;
			}
		}
		
		return true;
	}
	
	private boolean didPlayerWinAtMinorDiagonal(Mark player) {
		int row, col, lastIndex = this.boardWidth - 1;
		
		for (row = 0; row < this.boardWidth; ++row) {
			col = lastIndex - row;
			if (this.board[row][col] != player) {
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Get the mark at the specified tile of the board.
	 * @param row The tile's row, zero-based.
	 * @param col The tile's column, zero-based.
	 * @return The tile's mark.
	 */
	public Mark get(int row, int col) {
		return this.board[row][col];
	}
	
	/**
	 * Accessor for boardWidth.
	 * @return The board width.
	 */
	public int getBoardWidth() {
		return boardWidth;
	}

	/**
	 * Accessor for current player.
	 * @return The current player.
	 */
	public Mark getCurrentPlayer() {
		return currentPlayer;
	}

	/**
	 * Accessor for the winner.
	 * @return The winner. May be null.
	 */
	public Mark getWinner() {
		return winner;
	}
	
	/**
	 * The highest value for a tile's row or column.
	 * @return The highest index a tile's row or column can have for this game.
	 */
	public int getLastTile() {
		return this.boardWidth - 1;
	}
	
}
