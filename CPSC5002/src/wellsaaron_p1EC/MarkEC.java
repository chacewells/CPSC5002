package wellsaaron_p1EC;

public enum MarkEC {
	X("X"), O("O"), BLANK(" ");
	
	private String stringValue;
	
	private MarkEC(String stringValue) {
		this.stringValue = stringValue;
	}
	
	@Override
	public String toString() {
		return this.stringValue;
	}
}
