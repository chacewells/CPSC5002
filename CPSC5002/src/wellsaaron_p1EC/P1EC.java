/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_p1EC;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
 * Contains the main() method.
 * Should include the ability to repeat the game.
 * All of user input from the keyboard must be performed in this class
 * Uses the public methods of your TicTacToe class to test functionality (i.e. play the game).
 * Be sure to create functions that do one task and only one task.
 */

/**
 * 
 * @author wellsaaron
 *
 */
public class P1EC {
	
	public static void main(String[] args) throws IOException {
		displayWelcomeMessage();
		
		try (
				BufferedReader console = new BufferedReader(
						new InputStreamReader(System.in));
				) {
			TicTacToeEC game = getBoard(console);
			
			boolean done = false;
			while (!done) {
				nextTurn(console, game);
				
				if (game.isGameOver()) {
					game.displayGameEndMessage();

					done = !playAgain(console);
					if (!done) {
						game.reset();
					}
				}
			}
			
			thanksForPlaying();
		}
	}
	
	private static TicTacToeEC getBoard(BufferedReader console) 
			throws IOException {
		
		while (true) {
			try {
				int boardWidth = getBoardWidthFromUser(console);
				return new TicTacToeEC(boardWidth);
			} catch (NumberFormatException e) {
				System.out.println("That wasn't a valid integer! Please try "
						+ "again!");
			} catch (IllegalArgumentException e) {
				System.out.println(e.getMessage());
				System.out.println("Please try again!");
			}
		}
	}
	
	private static int getBoardWidthFromUser(BufferedReader console) 
			throws IOException {
		System.out.print("Please enter a board width: ");
		String userInput = console.readLine();
		return Integer.parseInt(userInput);
	}
	
	private static void displayWelcomeMessage() {
		System.out.println("Welcome to the TicTacToe game!");
		System.out.println("Two can play: X and O. O goes first.");
		System.out.println("You will be prompted for a row and column.");
		System.out.println("An 'X' or 'O' will be placed in the corresponding "
				+ "tile.");
		System.out.println("The player to play their Mark (X or O) across the "
				+ "entire board (horizontal, vertical or diagonal) wins!");
		System.out.println("Good luck!");
	}
	
	private static boolean playAgain(BufferedReader console) 
			throws IOException {
		System.out.println("Would you like to play again? (y/n) ");
		String userInput = console.readLine();
		
		return "y".equals(userInput);
	}
	
	private static void thanksForPlaying() {
		System.out.println("Thank you for playing! Goodbye!");
	}
	
	private static void nextTurn(BufferedReader console, TicTacToeEC game) {
		while (true) {
			try {
				game.displayBoard();
				game.printTurn();
				int row = getRowFromUser(console);
				int col = getColFromUser(console);
				
				if (!game.canPlayTile(row, col)) {
					System.out.println("That tile is already taken! "
							+ "Please try again!");
					continue;
				}
				game.playTile(row, col);
				
				return;
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			} catch (NumberFormatException|ArrayIndexOutOfBoundsException e) {
				System.out.println("The input was invalid. The row or column "
						+ "must be a positive integer between zero and " 
						+ game.getLastTile() + ".");
				
				continue;
			}
		}
	}


	private static int getRowFromUser(BufferedReader console) 
			throws IOException, NumberFormatException {
		return getRowOrColFromUser(console, "row");
	}
	
	private static int getColFromUser(BufferedReader console) 
			throws IOException, NumberFormatException {
		return getRowOrColFromUser(console, "column");
	}
	
	private static int getRowOrColFromUser(
			BufferedReader console, 
			String rowOrCol) 
			throws IOException, NumberFormatException {
		System.out.printf("Which %s? ", rowOrCol);
		String userInput = console.readLine();
		return Integer.parseInt(userInput);
	}
}
