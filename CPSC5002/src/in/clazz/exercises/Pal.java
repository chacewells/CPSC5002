/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package in.clazz.exercises;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/**
 * Short description of the class.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class Pal {
	
	public static void main(String[] args) {
		String[] strings = { "hello", "foo", "boob", "salas", "why" };
		for (String s : strings) {
			System.out.println(s + ": " + isPalindrome(s));
		}
	}
	
	static boolean isPalindrome(String word) {
		Queue<Character> wQueue = new LinkedList<>();
		Stack<Character> wStack = new Stack<>();
		for (char c : word.toCharArray()) {
			wQueue.add(c);
			wStack.push(c);
		}
		
		while (!wQueue.isEmpty()) {
			if (!wQueue.poll().equals(wStack.pop())) {
				return false;
			}
		}
		
		return true;
	}

}
