/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package in.clazz.exercises;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Short description of the class.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class Stutter {
	
	public static void main(String[] args) {
		Queue<Integer> kuayway = new LinkedList<>(Arrays.asList(1, 2, 3));
		stutter(kuayway);
		
		System.out.println(kuayway);
	}
	
	public static Queue<Integer> stutter(Queue<Integer> myQueue) {
		int size = myQueue.size();
		for (int i = 0; i < size; ++i) {
			int data = myQueue.poll();
			myQueue.add(data);
			myQueue.add(data);
		}
		
		return myQueue;
	}
}
