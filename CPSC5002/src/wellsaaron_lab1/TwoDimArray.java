/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_lab1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Random;

/**
 * TwoDimArray.java
 * Creates a two-dimensional square array with the user-requested size.
 * Fills all the elements of the array with random integers.
 * Prints the array contents along with the sum of each row, column and major diagonals.
 * Offers to repeat as many times as the user wishes.
 * 
 * @author aaron
 *
 */
public class TwoDimArray {
	private static Random random = new Random();
	private static final Integer UPPER_LIMIT_EXCLUSIVE = 100;
	
	private static final String GREETING = "Welcome to the Two-dimensional flexibility program! " +
			"This program asks for the \n" + 
			"size of a 2D square array of integers, then creates the square array, fills \n" + 
			"it with random numbers, then prints it out along with sums in both directions \n" + 
			"and along the diagonals.";

	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		try (BufferedReader console = new BufferedReader(new InputStreamReader(System.in))) {
			boolean done = false;
			
			System.out.println(GREETING);
			while (!done) {
				System.out.print("How many rows(between 1 and 10)? ");
				
				Integer arraySize = null;
				try {
					arraySize = getArraySizeFromUser(console);
				} catch (IllegalArgumentException e) {
					System.out.println("Input must be a valid integer between 1 and 10! Try again!");
					continue;
				}
				
				Integer[][] flexArray = createRandomArray(arraySize);
				printFlexArrayWithSums(flexArray);
				
				while (true) {
					System.out.print("Would you like to repeat (y/n)? ");
					String userInput = console.readLine();
					if ("n".equals(userInput)) {
						done = true;
						System.out.println("Goodbye!");
						break;
					} else if ("y".equals(userInput)) {
						System.out.println("Okay, one more time!");
						break;
					} else {
						System.out.println("I didn't understand that. Please answer 'y' or 'n'");
					}
				}
			}
		}
	}
	
	private static void printFlexArrayWithSums(Integer[][] flexArray) {
		Integer majorDiagonalSum = 0;
		Integer minorDiagonalSum = 0;
		Integer[] columnSums = new Integer[flexArray.length];
		Arrays.fill(columnSums, 0);
		
		int lastIndex = flexArray.length - 1;
		for (int row = 0; row <= lastIndex; ++row) {
			Integer rowSum = 0;
			System.out.printf("%-5s", "");
			
			majorDiagonalSum += flexArray[row][row];
			minorDiagonalSum += flexArray[row][lastIndex - row];
			for (int col = 0; col <= lastIndex; ++col) {
				Integer value = flexArray[row][col];
				columnSums[col] += value;
				rowSum += value;
				
				System.out.printf("% 5d", value);
			}
			System.out.printf(" =% 5d%n", rowSum);
		}
		
		System.out.printf("% 5d", minorDiagonalSum);
		for (Integer columnSum : columnSums) {
			System.out.printf("% 5d", columnSum);
		}
		System.out.printf("  % 5d%n%n", majorDiagonalSum);
	}

	private static Integer[][] createRandomArray(int size) {
		Integer[][] randomArray = new Integer[size][size];
		
		for (int row = 0; row < size; ++row) {
			for (int col = 0; col < size; ++col) {
				randomArray[row][col] = random.nextInt(UPPER_LIMIT_EXCLUSIVE);
			}
		}
		
		return randomArray;
	}
	
	private static Integer getArraySizeFromUser(BufferedReader console) throws IOException, IllegalArgumentException {
		Integer arraySize = null;
		System.out.print("How many rows (between 1 and 10)? ");
		String userInput = console.readLine();
		arraySize = Integer.parseInt(userInput);
		
		return arraySize;
	}

}
