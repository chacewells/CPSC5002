package wellsaaron_lab1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Random;

public class TwoDimArrayOO {
	public static final int MAX_BOUND = 100;
	
	private int[][] array2D;
	private int width, height; // optional
	private int[] horizSumArr; // optional
	
	/**
	 * constructor for 2D array of random numbers
	 * @param width
	 * @param height
	 */
	public TwoDimArrayOO(int width, int height) {
		this.width = width;
		this.height = height;
		this.array2D = new int[this.height][this.width];
		this.fillRandomValues();
		
		this.horizSumArr = new int[height];
		this.calcHorizSum();
	}
	
	/**
	 * constructor for 2D squared array of random numbers
	 * @param width
	 */
	public TwoDimArrayOO(int width) {
		this(width, width);
	}
	
	private void fillRandomValues() {
		Random random = new Random();
		
		for (int row = 0; row < height; ++row) {
			for (int col = 0; col < width; ++col) {
				this.array2D[row][col] = random.nextInt(MAX_BOUND);
			}
		}
	}
	
	private void calcHorizSum() {
		for (int row = 0; row < this.height; ++row) {
			for (int col = 0; col < this.width; ++col) {
				this.horizSumArr[row] += this.array2D[row][col];
			}
		}
	}
	
	public int sumAtRow(int row) {
		return horizSumArr[row];
	}
	
	private static int getRowsFromUser( ) {
		int result;
		try (BufferedReader console = new BufferedReader(new InputStreamReader(System.in))) {
			System.out.print("Enter a number less than 11: ");
			String userInput = console.readLine();
			result = Integer.parseInt(userInput);
		} catch (IOException e) {
			System.err.println("that was bad");
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		
		return result;
	}
	
	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();
		for (int row = 0; row < this.height; ++row) {
			buf.append(Arrays.toString(this.array2D[row]) + "; row sum " + this.horizSumArr[row] + "\n");
		}
		return buf.toString();
	}

	public static void main(String[] args) {
		int size = getRowsFromUser();
		TwoDimArrayOO arr = new TwoDimArrayOO(size, size);
		System.out.println(arr);
	}

}
