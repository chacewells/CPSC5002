/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_lab6;

/**
 * Represents the winner of a single play during War.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public enum RoundWinner { 
	// first player
	PLAYER_ONE, 
	// second player
	PLAYER_TWO, 
	// a tie
	TIE
}