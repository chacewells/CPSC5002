/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_lab6;

import java.util.stream.Stream;

/**
 * Encapsulates a play during a round of War. Indicates a winner, provides a
 * user-friendly message, and provides the cards played as a Stream.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class Play {
	// Player one's card
	private Integer playerOneCard;
	// Player two's card
	private Integer playerTwoCard;
	
	/**
	 * Constructor for a play during a round.
	 * 
	 * @param p1card Player one's card.
	 * @param p2card Player two's card.
	 */
	public Play(Integer p1card, Integer p2card) {
		playerOneCard = p1card;
		playerTwoCard = p2card;
	}

	/**
	 * Get both cards as a Stream. Convenience for slurping into a sequence
	 * that is later added to a player's hand.<br>
	 * <a href="https://tinyurl.com/q86j67m">Stream API</a>
	 * 
	 * @return A Stream of both players' cards.
	 */
	public Stream<Integer> allCards() {
		return Stream.of(playerOneCard, playerTwoCard);
	}

	/**
	 * Returns the winner of this play.
	 * 
	 * @return The winner of this play.
	 */
	public RoundWinner getWinner() {
		int playerDiff = playerOneCard - playerTwoCard;
		if (playerDiff > 0) {
			return RoundWinner.PLAYER_ONE;
		} else if (playerDiff < 0) {
			return RoundWinner.PLAYER_TWO;
		} else {
			return RoundWinner.TIE;
		}
	}
	
	/**
	 * A user-friendly message displaying the winner.
	 * 
	 * @return A message informing the user who collects all cards.
	 */
	public String getCollectCardsMessage() {
		// Display this when no one wins; should never be printed
		final String noOne = "No one";
		// this round's winner
		String winner = null;
		
		switch (getWinner()) {
		case PLAYER_ONE:
			winner = GameModel.PLAYER_ONE;
			break;
		case PLAYER_TWO:
			winner = GameModel.PLAYER_TWO;
			break;
		case TIE:
			winner = noOne;
			break;
		}
		
		return winner + " collects all cards.";
	}
	
	@Override
	public String toString() {
		return String.format(
				"Player 1: %d, Player 2: %d",
				playerOneCard,
				playerTwoCard);
	}
}
