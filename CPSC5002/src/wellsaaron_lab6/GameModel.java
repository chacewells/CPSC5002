/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_lab6;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.function.IntFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Game logic for a game of War.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class GameModel {
	/**
	 * A game of War is always initialized with this deck size.
	 */
	public static final Integer DECK_SIZE = 52;
	public static final String PLAYER_ONE = "Player 1";
	public static final String PLAYER_TWO = "Player 2";
	
	// The deck
	private Stack deck;
	// Player one's hand
	private Queue playerOneHand;
	// Player two's hand
	private Queue playerTwoHand;
	
	/**
	 * Initializes a new game of War.
	 */
	public GameModel() {
		initializeDeck();
		playerOneHand = new Queue();
		playerTwoHand = new Queue();
	}
	
	/**
	 * Deals 26 cards each to player one and player two.
	 */
	public void dealCards() {
		System.out.println("Dealing cards...");
		
		while (!deck.empty()) {
			deal(deck, playerOneHand);
			deal(deck, playerTwoHand);
		}
	}
	
	/**
	 * Displays the number of cards in each player's hand.
	 */
	public void displayGameState() {
		System.out.println(getGameState() + "\n");
	}
	
	// String describing the number of cards in each player's hand
	private String getGameState() {
		return String.format("Player 1 has %d cards, player 2 has %d cards",
				playerOneHand.size(), playerTwoHand.size());
	}
	
	/**
	 * Play a round of War.<br>
	 * Each player removes the top card from his/her deck.<br>
	 * The player with the highest card puts both cards back into his/her deck.
	 * <br>
	 * In the event of a tie, another card is played until a winner emerges.
	 */
	public void play() {
		// all plays
		List<Play> plays = new LinkedList<>();
		// the current play
		Play play = null;
		boolean somebodyWon = false;
		
		while (!somebodyWon) {
			play = new Play(playerOneHand.dequeue(), playerTwoHand.dequeue());
			System.out.println(play);
			plays.add(play);
			somebodyWon = play.getWinner() != RoundWinner.TIE;
		}
		
		System.out.println(play.getCollectCardsMessage());
		returnCards(play, plays);
		displayGameState();
	}
	
	// Return all cards on the table to the round's winner
	// Using functional API for succinctness: https://tinyurl.com/q86j67m
	private void returnCards(Play lastPlay, List<Play> plays) {
		Queue selectedPlayer = lastPlay.getWinner() == RoundWinner.PLAYER_ONE
				? playerOneHand
				: playerTwoHand;
		
		plays.stream()
			.flatMap(Play::allCards)
			.forEach(selectedPlayer::enqueue);
	}
	
	/**
	 * Indicates whether the game is over.
	 * 
	 * @return True if there is a winner; false otherwise.
	 */
	public boolean over() {
		return playerOneHand.empty() || playerTwoHand.empty();
	}
	
	// Deal a card from the deck to a player's hand
	private static void deal(Stack deck, Queue hand) {
		hand.enqueue(deck.pop());
	}
	
	// Fill the deck with 52 cards
	// Using Functional API for succinctness: https://tinyurl.com/q86j67m
	private void initializeDeck() {
		ArrayList<Integer> regularDeck = getUnshuffledDeck();
		shuffleDeck(regularDeck);
		deck = new Stack(DECK_SIZE);
		
		regularDeck.forEach(deck::push);
	}
	
	// Get an unshuffled deck of cards as an ArrayList<Integer>
	// Using functional API for succinctness: https://tinyurl.com/y8v82u6q
	private ArrayList<Integer> getUnshuffledDeck() {
		IntFunction<IntStream> fourTimes = i -> IntStream.of(i, i, i, i);
		
		List<Integer> deck = IntStream
				.range(1, 14)
				.flatMap(fourTimes)
				.mapToObj(Integer::valueOf)
				.collect(Collectors.toList());
		
		return new ArrayList<>(deck);
	}
	
	/**
	 * Shuffles the cards using the
	 * <a href="https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle">
	 * Fisher-Yates algorithm</a>
	 * @param cards deck to shuffle
	 */
	private void shuffleDeck(ArrayList<Integer> cards) {
	    Random rand = new Random();
	    for (int i = cards.size(); i > 1; i--) {
	        int j = rand.nextInt(i);
	        int temp = cards.get(i - 1);
	        cards.set(i - 1, cards.get(j));
	        cards.set(j, temp);
	    }
	}

	/**
	 * Displays the winner of this entire game. The winner is the one who
	 * doesn't have zero cards in his/her deck.
	 */
	public void displayGrandWinner() {
		if (!over()) {
			throw new IllegalStateException("The game's not over yet!");
		}
		
		String winner = playerOneHand.empty() ? PLAYER_TWO : PLAYER_ONE;
		
		System.out.printf("Congratulations, %s. You've won!", winner);
	}

}
