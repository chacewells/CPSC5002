/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_lab6;

import java.util.EmptyStackException;

/**
 * Implements a fixed size stack data structure.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class Stack {
	private Integer[] stack;
	private int top;
	
	/**
	 * Constructs a fixed-size stack of the specified size.
	 * 
	 * @param size The size of the stack. 
	 */
	public Stack(int size) {
		stack = new Integer[size];
	}
	
	/**
	 * Adds an integer to the stack.
	 * 
	 * @param data The integer to add.
	 * @throws NullPointerException When the user attempts to pass null.
	 * @throws IndexOutOfBoundsException When this stack is full and cannot
	 * accept any more elements.
	 */
	public void push(Integer data) {
		if (data == null) {
			throw new NullPointerException("Data cannot be null");
		}
		if (full()) {
			throw new IndexOutOfBoundsException("The stack is full");
		}
		
		stack[top++] = data;
	}
	
	/**
	 * Removes and returns the top of the stack.
	 * 
	 * @return The value at the top of the stack.
	 */
	public Integer pop() {
		Integer data = peek();
		stack[--top] = null;
		
		return data;
	}
	
	/**
	 * Returns the top of the stack without removing it.
	 * 
	 * @return The top of the stack.
	 * @throws EmptyStackException if this stack is empty.
	 */
	public Integer peek() {
		if (empty()) {
			throw new EmptyStackException();
		}
		
		return stack[top - 1];
	}
	
	/**
	 * Whether this stack is empty.
	 * 
	 * @return True if the stack is empty; false otherwise.
	 */
	public boolean empty() {
		return top == 0;
	}
	
	/**
	 * Whether this stack is full.
	 * 
	 * @return True if the stack is full of elements.
	 */
	public boolean full() {
		return top == stack.length;
	}
	
	/**
	 * Gets the size of the stack.
	 * 
	 * @return The size of the stack.
	 */
	public int size() {
		return top;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder("<");
		for (int i = top - 1; i >= 0; --i) {
			builder.append(stack[i]).append("<");
		}
		
		return builder.toString();
	}
	
}
