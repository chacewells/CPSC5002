/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_lab6;

/**
 * Plays a game of War between two players and prints the result to the user.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class WarGame {
	
	/**
	 * See class JavaDoc.
	 * 
	 * @param args No program arguments expected.
	 */
	public static void main(String[] args) {
		GameModel game = new GameModel();
		game.dealCards();
		
		while (!game.over()) {
			game.play();
		}
		
		game.displayGrandWinner();
	}

}
